// Regulate the rrbot double inverted pendulum in gazebo.
// This code serves as the "middleman" between gazebo and the controller,
// translating messages into the appropriate format for each.

#include <ros/ros.h>
#include <gazebo_msgs/ApplyJointEffort.h>
#include "pid/plant_msg.h"
#include "pid/controller_msg.h"
#include "sensor_msgs/JointState.h"
#include "rosgraph_msgs/Clock.h"

void control_effort_cb(const pid::controller_msg& u_msg);
void joint_angle_cb(const sensor_msgs::JointState& Gazebo_joint_angles);

///////////////////////////////////////////////////////////
// Global variables
///////////////////////////////////////////////////////////

// These must be global b/c there's no other way to pass values out of a ROS callback
sensor_msgs::JointState joint_angles; // Stores the joint angles from Gazebo
pid::controller_msg u; // Stores the motor commands from the controller
bool controller_is_active = false; // Halts motor commands if we don't hear anything from controller


int main(int argc, char** argv)
{
  ///////////////////////////////////////////////////////////
  // Local variables
  ///////////////////////////////////////////////////////////
  ros::init(argc, argv, "pendulum_middleman");

  // Spawn the node
  ros::NodeHandle middleman;

  double delta_t = 0.0001; // Control rate
  ros::Rate loop_rate(1./delta_t);
  double setpoint = 3141.59;		// Target position is straight up
 					// Angular vel target to be set later

  // Used to send joint efforts to Gazebo
  gazebo_msgs::ApplyJointEffort J1_effort;

  // The message that will be sent to the PID controller
  pid::plant_msg  msg_to_controller; 


  ///////////////////////////////////////////////////////////
  // Initialize variables
  ///////////////////////////////////////////////////////////
  // 1 joint
  joint_angles.position.push_back(0.0);
  joint_angles.velocity.push_back(0.0);
  u.u = 0.0;


  ///////////////////////////////////////////////////////////
  // Publishers and subscribers
  ///////////////////////////////////////////////////////////

  // Subscribe to control effort from PID controller
  ros::Subscriber effort_sub = middleman.subscribe("control_effort", 1, control_effort_cb );

  // Publish (joints, time, setpoint) to PID controller
  ros::Publisher chatter_pub = middleman.advertise<pid::plant_msg>("state", 1);

  // Client for sending joint efforts to Gazebo
  ros::ServiceClient joint_effort_client = middleman.serviceClient<gazebo_msgs::ApplyJointEffort>("/gazebo/apply_joint_effort");

  // Subscribe to hear joints from Gazebo
  ros::Subscriber joint_sub = middleman.subscribe("/rrbot/joint_states", 1, joint_angle_cb);

  while( ros::ok() )
  {
    // Will the controller return a value? Reset, then check after spinning
    controller_is_active = false;

    ros::spinOnce();

    // Listen for (t,x) to be published from gazebo.
    // Where x (the state) is the joint angles
    // It defaults to simulation time if this node is spawned after Gazebo
    //ROS_INFO("Time:  %6.4f", ros::Time::now().toSec() );
    ROS_INFO("Joints: %f", joint_angles.position.at(0) );

    
    // Publish (t,x,setpoint) to the controller so it can calculate the next control effort.
    msg_to_controller.x = joint_angles.position.at(0); //Pendulum angle
    msg_to_controller.t = ros::Time::now().toSec();
    msg_to_controller.setpoint = setpoint;

    chatter_pub.publish(msg_to_controller);

    // Publish the u (control effort) message from the controller to Gazebo.
    // Only publish if we got a response from the controller
    if ( controller_is_active )
    {
      J1_effort.request.duration = ros::Duration(delta_t);
      J1_effort.request.joint_name = "joint1";
      J1_effort.request.effort = u.u;

      joint_effort_client.call(J1_effort);
    }


    
    loop_rate.sleep();
  }

  ros::shutdown();

  return 0;
}

// Callback when the controller publishes 'control_effort'
void control_effort_cb(const pid::controller_msg& u_msg)
{
  // Store the control effort from the topic to a global variable
  u.u = u_msg.u;
  
  // Signal that the controller did return a value
  controller_is_active = true;
}

// Callback when Gazebo publishes joint angles
void joint_angle_cb(const sensor_msgs::JointState& Gazebo_joint_angles)
{
  // Record the joint angles from Gazebo to a global variable
  // In Gazebo, 0 is at the top.
  // But for the dynamic model, 0 is at the bottom.
  // So add pi to the angle from gazebo

  // Also convert to mrad so that angular error is corrected faster

  // Also convert to an angle between -180deg and 180deg with fmod()
  joint_angles.position.at(0) = 1000.*( Gazebo_joint_angles.position.at(0)+3.14159 );

}
